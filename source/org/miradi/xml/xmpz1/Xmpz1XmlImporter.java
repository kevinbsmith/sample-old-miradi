/* 
Copyright 2005-2010, Foundations of Success, Bethesda, Maryland 
(on behalf of the Conservation Measures Partnership, "CMP") and 
Beneficent Technology, Inc. ("Benetech"), Palo Alto, California. 

This file is part of Miradi

Miradi is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3, 
as published by the Free Software Foundation.

Miradi is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Miradi.  If not, see <http://www.gnu.org/licenses/>. 
*/ 

package org.miradi.xml.xmpz1;

import java.util.Vector;


import org.miradi.main.EAM;
import org.miradi.objecthelpers.ORef;
import org.miradi.objecthelpers.ThreatStressRatingEnsurer;
import org.miradi.project.Project;
import org.miradi.questions.ChoiceQuestion;
import org.miradi.utils.HtmlUtilities;
import org.miradi.utils.ProgressInterface;
import org.miradi.utils.XmlUtilities2;
import org.miradi.xml.AbstractXmlImporter;
import org.miradi.xml.AbstractXmlNamespaceContext;
import org.miradi.xml.AbstractXmpzObjectImporter;
import org.miradi.xml.MiradiXmlValidator;
import org.miradi.xml.wcs.TagToElementNameMap;
import org.miradi.xml.wcs.WcsMiradiXmlValidator;
import org.miradi.xml.wcs.Xmpz1XmlConstants;
import org.w3c.dom.Node;

public class Xmpz1XmlImporter extends AbstractXmlImporter implements Xmpz1XmlConstants
{
	public Xmpz1XmlImporter(Project projectToFill, ProgressInterface progressIndicatorToUse) throws Exception
	{
		super(projectToFill);
		
		progressIndicator = progressIndicatorToUse;
	}
	
	@Override
	protected void importXml() throws Exception
	{
		Vector<AbstractXmpzObjectImporter> importers = new Vector<AbstractXmpzObjectImporter>();
		
		importers.add(new ProjectSummaryImporter(this));
		importers.add(new ProjectResourceImporter(this));
		importers.add(new OrganizationImporter(this));
		importers.add(new ProjectSummaryScopeImporter(this));
		importers.add(new ProjectSummaryLocationImporter(this));
		importers.add(new ProjectSummaryPlanningImporter(this));
		importers.add(new TncProjectDataImporter(this));
		importers.add(new WwfProjectDataImporter(this));
		importers.add(new WcsProjectDataImporter(this));
		importers.add(new RareProjetDataImporter(this));
		importers.add(new FosProjectDataImporter(this));
		
		importers.add(new ConceptualModelPoolImporter(this));
		importers.add(new ResultsChainDiagramPoolImporter(this));
		
		importers.add(new CausePoolImporter(this));
		importers.add(new BiodiversityTargetPoolImporter(this));
		importers.add(new HumanWelfareTargetPoolImporter(this));
		importers.add(new StrategyPoolImporter(this));
		importers.add(new ThreatReductionResultsPoolImporter(this));
		importers.add(new IntermediateResultPoolImporter(this));
		importers.add(new StressPoolImporter(this));
		importers.add(new ScopeBoxPoolImporter(this));
		importers.add(new TextBoxPoolImporter(this));
		importers.add(new GroupBoxPoolImporter(this));
		importers.add(new TaskPoolImporter(this));		
		
		importers.add(new DiagramFactorPoolImporter(this));
		importers.add(new DiagramLinkPoolImporter(this));
		
		importers.add(new KeyEcologicalAttributePoolImporter(this));
		importers.add(new IndicatorPoolImporter(this));
		importers.add(new GoalPoolImporter(this));
		importers.add(new ObjectivePoolImporter(this));
		importers.add(new MeasurementPoolImporter(this));
		importers.add(new SubTargetPoolImporter(this));
		importers.add(new ProgressReportPoolImporter(this));
		importers.add(new ProgressPercentPoolImporter(this));
		importers.add(new AccountingCodePoolImporter(this));
		importers.add(new FundingSourcePoolImporter(this));
		importers.add(new BudgetCategoryOnePoolImporter(this));
		importers.add(new BudgetCategoryTwoPoolImporter(this));
		importers.add(new IucnRedListspeciesPoolImporter(this));
		importers.add(new OtherNotableSpeciesPoolImporter(this));
		importers.add(new AudiencePoolImporter(this));
		importers.add(new ObjectTreeTableConfigurationPoolImporter(this));
		importers.add(new ExpenseAssignmentPoolImporter(this));
		importers.add(new ResourceAssignmentPoolImporter(this));
		importers.add(new DashboardPoolImporter(this));
		importers.add(new TaggedObjectSetPoolImporter(this));
		importers.add(new ExtraDataImporter(this));
		
		final int DELETE_ORPHANS_PROGRESS_TASK = 1;
		final int IMPORT_THREAT_STRESS_RATING_PROGERESS_TASK = 1;
		final int TOTAL_CUSTOM_TASK_COUNT = DELETE_ORPHANS_PROGRESS_TASK + IMPORT_THREAT_STRESS_RATING_PROGERESS_TASK;
		progressIndicator.setStatusMessage(EAM.text("Importing XML..."), importers.size() + TOTAL_CUSTOM_TASK_COUNT);
		for (AbstractXmpzObjectImporter importer : importers)
		{
			importer.importElement();
			incrementProgress();
		}
		
		importThreatStressRatings();
		incrementProgress();
		
		importDeletedOrphanText();
		incrementProgress();
	}

	private void incrementProgress() throws Exception
	{
		progressIndicator.incrementProgress();
	}

	private void importDeletedOrphanText() throws Exception
	{
		Node node = getNamedChildNode(getRootNode(), Xmpz1XmlConstants.DELETED_ORPHANS_ELEMENT_NAME);
		String nodeContent = getSafeNodeContent(node);
		nodeContent = XmlUtilities2.getXmlEncoded(nodeContent);
		nodeContent = HtmlUtilities.replaceNonHtmlNewlines(nodeContent);
		getProject().appendToQuarantineFile(nodeContent);
	}

	private void importThreatStressRatings() throws Exception
	{
		beginUsingCommandsToSetData();
		try
		{
			ThreatStressRatingEnsurer ensurer = new ThreatStressRatingEnsurer(getProject());
			ensurer.createOrDeleteThreatStressRatingsAsNeeded();
		}
		finally
		{
			endUsingCommandsToSetData();
		}
		
		new ThreatTargetThreatRatingElementImporter(this).importElement();
	}
	
	public void importCodeField(Node node, String containerName, ORef destinationRef, String destinationTag, ChoiceQuestion question) throws Exception
	{
		TagToElementNameMap map = new TagToElementNameMap();
		String elementName = map.findElementName(containerName, destinationTag);
		String importedReadableCode = getPathData(node, new String[]{containerName  + elementName, });
		String trimmedImportedReadbleCode = importedReadableCode.trim();
		String internalCode = question.convertToInternalCode(trimmedImportedReadbleCode);		
		importField(destinationRef, destinationTag, internalCode);
	}

	private void endUsingCommandsToSetData()
	{
		getProject().endCommandSideEffectMode();
	}

	private void beginUsingCommandsToSetData()
	{
		getProject().beginCommandSideEffectMode();
	}

	@Override
	protected String getNameSpaceVersion()
	{
		return NAME_SPACE_VERSION;
	}

	@Override
	protected String getPartialNameSpace()
	{
		return PARTIAL_NAME_SPACE;
	}

	@Override
	protected String getRootNodeName()
	{
		return CONSERVATION_PROJECT;
	}
	
	@Override
	public AbstractXmlNamespaceContext getNamespaceContext()
	{
		return new Xmpz1NameSpaceContext();
	}
	
	@Override
	protected MiradiXmlValidator createXmlValidator()
	{
		return new WcsMiradiXmlValidator();
	}

	@Override
	public void setData(ORef ref, String tag, String data) throws Exception
	{
		getProject().setObjectData(ref, tag, HtmlUtilities.replaceNonHtmlNewlines(data));
	}
	
	protected ProgressInterface progressIndicator;
}

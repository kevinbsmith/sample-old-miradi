# sample-old-miradi

This repo contains a copy of an application that I co-wrote back in 2005-2010. 
I do not recommend that anyone actually use this code,
since it hasn't been updated in years. If you are interested in 
a newer version of Miradi, check out <https://miradi.org>. 

This project was licensed under the GPL.
However, the source code was only released to people who paid for a license, 
so I would appreciate if it not be widely distributed (even though doing so would be legal). 
This scheme was a novel way to monetize work that was done using an open source license. 
Note that the Miradi name and trademark are reserved by their owners, 
and may not be used by anyone who uses this code. 

I was the architect and technical lead throughout the project. 
I probably wrote close to half of the code, and I carefully 
code reviewed and guided development of the rest.
All development was done by the non-profit that employed me; 
we never sought or received code contributions from external parties. 

Miradi is/was a cross-platform desktop application, that allowed
environmental conservation workers to plan their projects. 
It includes a graphical diagramming tool, 
as well as a number of data entry dialogs. 

The entry point for the application is `source/miradi/main/EAM.java`
(named EAM for historical reasons). The actual main loop (`run`) 
can be found in `source/miradi/main/miradi.java`. 
The application saves projects in a simple proprietary NoSQL data format. 
We didn't want to host a local SQL server, 
and the diagram data is very object/tree oriented, not tabular. 


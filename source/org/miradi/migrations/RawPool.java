/* 
Copyright 2005-2013, Foundations of Success, Bethesda, Maryland 
(on behalf of the Conservation Measures Partnership, "CMP") and 
Beneficent Technology, Inc. ("Benetech"), Palo Alto, California. 

This file is part of Miradi

Miradi is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3, 
as published by the Free Software Foundation.

Miradi is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Miradi.  If not, see <http://www.gnu.org/licenses/>. 
*/ 

package org.miradi.migrations;

import java.util.HashMap;

import org.miradi.objecthelpers.ORef;
import org.miradi.objecthelpers.ORefList;
import org.miradi.objecthelpers.ORefSet;

public class RawPool extends HashMap<ORef, RawObject>
{
	public RawObject findObject(ORef ref)
	{
		return get(ref);
	}
	
	public RawObject createObject(ORef ref)
	{
		RawObject newObject = new RawObject(ref);
		put(ref, newObject);
		
		return newObject;
	}
	
	public void deleteObject(ORef ref)
	{
		remove(ref);
	}
	
	public ORefList getSortedReflist()
	{
		ORefList refList = new ORefSet(keySet()).toRefList();
		refList.sort();
		
		return refList;
	}
}

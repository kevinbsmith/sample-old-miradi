/* 
Copyright 2005-2009, Foundations of Success, Bethesda, Maryland 
(on behalf of the Conservation Measures Partnership, "CMP") and 
Beneficent Technology, Inc. ("Benetech"), Palo Alto, California. 

This file is part of Miradi

Miradi is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3, 
as published by the Free Software Foundation.

Miradi is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Miradi.  If not, see <http://www.gnu.org/licenses/>. 
*/ 
package org.miradi.views.planning.doers;

import org.miradi.actions.ActionCreateChildTask;
import org.miradi.actions.ActionCreateFutureStatus;
import org.miradi.actions.ActionCreateIndicatorMeasurement;
import org.miradi.actions.ActionCreateSameLevelTask;
import org.miradi.actions.ActionTreeCreateActivity;
import org.miradi.actions.ActionTreeCreateIndicator;
import org.miradi.actions.ActionTreeCreateMethod;
import org.miradi.actions.ActionTreeCreateObjective;
import org.miradi.actions.ActionTreeShareActivity;
import org.miradi.actions.ActionTreeShareMethod;
import org.miradi.views.umbrella.doers.AbstractPopDownMenuDoer;

public class PlanningTreeNodeCreationMenuDoer extends AbstractPopDownMenuDoer
{
	@Override
	protected Class[] getAllPossibleActionClasses()
	{
		return new Class[] {
				ActionTreeCreateObjective.class,
				ActionTreeCreateIndicator.class,
				null,
				ActionTreeCreateActivity.class,
				ActionTreeShareActivity.class,
				null,
				ActionTreeCreateMethod.class,
				ActionTreeShareMethod.class,
				null,
				ActionCreateChildTask.class,			
				ActionCreateSameLevelTask.class,
				null,
				ActionCreateIndicatorMeasurement.class,
				ActionCreateFutureStatus.class,
			};
	}
}
